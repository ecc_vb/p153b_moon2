﻿' Project:P153B_Moon2
' Auther:IE2A No.24, 村田直人
' Date: 2015年06月05日


Public Class Form1

    Dim ButtonTexts() As String = {"スタート", "ストップ"} 'ボタンのテキスト
    Const BoxSize As Integer = 50       'picturboxのサイズ
    Const PanelWidth As Integer = 400   'フォームの横幅
    Dim BackColors() As Color = {Color.DarkBlue, Color.MediumBlue, Color.Blue,
                                Color.DodgerBlue, Color.LightSkyBlue}
    
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load



        Panel1.BackColor = BackColors(4)            '拝啓この手紙(拝啓)
        Me.ClientSize = New Size(PanelWidth, 170)   'フォームの幅の設定
        Panel1.SetBounds(0, 0, PanelWidth, 120)     'パネルの位置とサイズを設定

        'pictureboxの設定
        With PictureBox1
            .SetBounds(-BoxSize, 30, BoxSize, BoxSize)  '位置とサイズを指定
            .Image = ImageList1.Images(0)               'imageの設定
        End With

        'ボタンの設定
        With StartButton
            .Location = New Point((PanelWidth - .Size.Width) \ 2, 130)  'ロケーション設定
            .Text = ButtonTexts(0)                                      'テキスト設定
        End With


    End Sub

    Private Sub StartButton_Click(sender As Object, e As EventArgs) Handles StartButton.Click

        Static s As Integer '「スタート」と「ストップ」の切り替えフラグ

        'タイマーの「起動」,「停止」処理
        If s = 0 Then
            Timer1.Enabled = True               'タイマー起動
            s = 1                               'フラグ更新
            StartButton.Text = ButtonTexts(s)   'ボタンのテキストを設定
        ElseIf s = 1 Then
            Timer1.Enabled = False              'タイマー停止
            s = 0                               'フラグ更新
            StartButton.Text = ButtonTexts(s)   'ボタンのテキストを設定
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick

        'pictuerboxの移動
        With PictureBox1
            .Location = New Point(.Location.X + 5, .Location.Y) 'ロケーション設定

            If (.Location.X >= PanelWidth) Then
                .Location = New Point(-BoxSize, .Location.Y)
            End If

        End With

        Static t As Integer 'カウンタ
        t = t + 1           'インクリメント

        '初期化判断
        If t = 90 Then
            t = 0
        End If

        If t Mod 10 = 0 Then
            PictureBox1.Image = ImageList1.Images(t \ 10)
        End If

    End Sub
End Class
